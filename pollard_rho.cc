/**
f(x) = x^2 + c
*/

bool isPowOf2(int n){ return (n&(n-1)); }

// linear gcd | faster than __gcd()
long long gcd(long long a,long long b){
    if(a==0)return 1;
    if(a<0) return gcd(-a,b);
    while(b){
        long long t=a%b;
        a=b;
        b=t;
    }
    return a;
}

// O(n^(1/4)
long long PollardBrent(long long num,long long c){
    long long x,y,p;
    x = p = 1LL;
    int next = 2,i=1;
    while (p == 1){
        ++i;
        if (i == next){
            y = x;
            next *= 2;
        }
        x = (mulmod(x,x,num) + c) % num;
        if (y == x) return PollardRho(num,c+1);
        p = gcd(num,llabs(x-y));
    }
    return p;
}


// O(n^(1/4)
// in main()  ->  srand(time(NULL));
long long rho(long long n){
    long long i=1,k=2,x=rand()%n,y=x,c=rand()%(n-1)+1;
    while(1){
        i++;
        x=(mulmod(x,x,n)+c)%n;
        long long d = gcd(y-x+n,n);
        if(d!=1 && d!=n) return d;
        if(y==x) return n;
        if(i==k) y=x,k<<=1;
    }
}


// O(n^(1/4)
map<long long,int> fact;
void factorize(long long n){
    if(n==1) return;
    if(is_prime_mr(n)){
        fact[n]++;
        return;
    }

    long long x=n;
    while(x>=n){
      x=PollardBrent(x,1);
    }

    factorize(x);
    factorize(n/x);
}







/**
Other Ref:
----------

http://xn--2-umb.com/09/12/brent-pollard-rho-factorisation
https://comeoncodeon.wordpress.com/2010/09/18/pollard-rho-brent-integer-factorization/
http://www.geeksforgeeks.org/pollards-rho-algorithm-prime-factorization/
https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm#Variants
*/