#include <bits/stdc++.h>
using namespace std;

int coin[1000];
long long mem[1000];
// n=amount to create
// m=number of coin types
long long rec(int n,int m){
    memset(mem,0,sizeof mem);
    
    mem[0]=1;
    for(int i=0;i<m;i++){
        for(int j=coin[i];j<=n;j++){
            mem[j]+=mem[j-coin[i]];
        }
    }
    return mem[n];
}
int main() {
    int n,m;
    cin>>n>>m;
    for(int i=0;i<m;i++) cin>>coin[i];
    cout<<rec(n,m)<<endl;
    return 0;
}