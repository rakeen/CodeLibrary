/*
returns a simple DFS tree
usecase: floodfill, topsort, connected components, strongly connected components, finding bi-connectivity, finding bridges in graph
*/
vector<int> G[MAXN];
bool vis[MAXN];
void dfs(int u){
    vis[u]=1;
    for(auto i:G[u].size()) if(!vis[G[u][i]]) dfs(G[u][i]);
}


/*
DFS that calcs something while traversing
On Directed Graph
If there's a circle it wont detect.
Perfect for ring shaped graph.
e.g. LOJ1049
*/
void dfs(int s,int p,int &res){
	l(i,G[s].size()){
		if(G[s][i].first!=p){
			res+=G[s][i].second;
			if(G[s][i].first!=1) dfs(G[s][i].first,s,res); // assuming 1 is the source
		}
	}
}
dfs(1,G[1][0]) // start from 1 with parent as its first adj node



/*
White-Gray-Black DFS
usecase: bridge detection, 
*/
vector<int> G[MAXN];
int color[MAXN]; // 0=white 1=gray 2=black
int dfsNum[MAXN],num; // dfsNum is the arrival/discovery time
// p is the parent of u
void dfs(int u,int p){
    color[u]=1;
    dfsNum[u]=num++;
    for(auto i:G[u].size()){
        int v=G[u][i];
        if(v==p) continue;
        if(color[v]==0) dfs(v,u); // (u,v) is a forward edge
        else if(color[v]==1);     // (u,v) is a back edge
        else ;                    // (u,v) is a cross edge
    }
    color[u]=2;
}