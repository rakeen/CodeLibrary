/*
O(V+E)

Lowlink: LOWLINK (v) is the smallest vertex which is in the same component as v
and is reachable by traversing zero or more tree arcs followed by at most one
frond or cross-link.
scc[i]  scc of i-th node

ref:
https://gist.github.com/nhocki/9f34696b473ced99f2c4
https://github.com/PetarV-/Algorithms/blob/master/Graph%20Algorithms/Tarjan's%20SCC%20Algorithm.cpp
*/
vector<int> G[MAXN];
int arrival[MAXN],low[MAXN],scc[MAXN]; // low link
bool stacked[MAXN];
int currentSCC;
void dfs(int u){
    static int ticks=0;
    arrival[u]=low[u]=ticks++;
    s.push(u);
    stacked[u]=1;
    for(auto i:G[u]){
        int v=G[u][i];
        if(arrival[v]!=-1){
            dfs(v);
            low[u]=min(low[u],low[v]):
        }
        else if(stacked[u]) low[u]=min(low[u],lov[v]);
    }
    if(arrival[u]==low[u]){
        int v;
        do{
            v=s.top();
            s.pop();
            stacked[v]=0;
            scc[v]=currentSCC;
        }while(u!=v);
        currentSCC++;
    }
}
void tarjan(){
    s.clear();
    memset(stacked,0,sizeof stacked);
    memset(low,0,sizeof low);
    memset(arrival,-1,sizeof arrival);
    
    currentSCC=0;
    for(int i=0;i<V;i++){ // V is the total number of vertex
        if(arrival[i]==-1) dfs(i);
    }
}