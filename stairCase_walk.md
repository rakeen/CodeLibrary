# [StaircaseWalk](http://mathworld.wolfram.com/StaircaseWalk.html)  

The number of staircase walks on a grid with m horizontal lines and n vertical lines is given by

```
  C(m+n,m)=((m+n)!)/(m!*n!) 
```

The first few values for m=n=1, 2, ..., are 1, 2, 6, 20, 70, 252, ... which are the central binomial coefficients.


### More:

1. http://joaoff.com/2008/01/20/a-square-grid-path-problem/
2. http://betterexplained.com/articles/navigate-a-grid-using-combinations-and-permutations/

<br>

# [DyckPath](http://mathworld.wolfram.com/DyckPath.html)  

A Dyck path is a staircase walk from (0,0) to (n,n) that lies strictly below (but may touch) the diagonal y=x. The number of Dyck paths of order n is given by the Catalan number

```
  Catalan_n = C(2n,n)/(n+1)
```


### Related:

- [FerrersDiagram](http://mathworld.wolfram.com/FerrersDiagram.html)
- [Young Diagram](https://en.wikipedia.org/wiki/Young_tableau)

<br>

# [Balls in Urns/Stars and Bars](http://www.artofproblemsolving.com/wiki/index.php/Ball-and-urn)

How many ways can one distribute **k** indistinguishable objects into **n** bins? We can imagine this as finding the number of ways to drop **k** balls into **n** urns, or equivalently to drop **k** balls amongst **n-1** dividers. The number of ways to do such is(given there can be 0 balls in an urn) 
```
  C(n+k-1,k)
```

However, if there can only be a positive amount of balls in an urn, then the number of ways to do such is,
```
  C(n-1,k-1)
```

### Related:

- [uva 10943](https://uva.onlinejudge.org/external/109/10943.pdf)