// harmonic number sum(1+1/2+1/3...) is log(n)
// sum of reciprocals of prime is log(log(n))
// ref: https://stackoverflow.com/a/2582776/4437655

#define LIM 10000000

bool vis[LIM+1];
vector<int> prime;
void sieve(){
	vis[2]=1;
	prime.push_back(2);
	for(int i=3;i<=sqrt(LIM);i+=2){
		if(!vis[i]){
			prime.push_back(i);
			for(int j=i*i;j<LIM;j+=i){
				vis[j]=1;
			}
		}
	}

    // or you can simply start from i=0 over vis to avoid this sqrt thing
	int x = sqrt(LIM);
	if(x%2==0) x++; // because even means non-prime
	for(int i=x;i<LIM;i+=2){
		if(!vis[i]) prime.push_back(i);
	}
}

// segmented sieve: http://zobayer.blogspot.com/2009/09/segmented-sieve.html