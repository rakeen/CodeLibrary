A = i + b/2 -1
i = (2*A + 2 - b)/2

i = inner
b = border

A = area

PointsOnLine(x0, y0, x1, y1) = GCD(Abs(x2-x1), Abs(y2-y1))

For all edges of a triangle:

b = PointsOnLine(V[0].x, V[0].y, V[1].x, V[1].y) + 
    PointsOnLine(V[1].x, V[1].y, V[2].x, V[2].y) + 
    PointsOnLine(V[0].x, V[0].y, V[2].x, V[2].y)
    

Reference:

http://stackoverflow.com/a/31211323
http://jwilson.coe.uga.edu/emat6680fa05/schultz/6690/pick/pick_main.htm