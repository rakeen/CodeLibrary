// sum of divisor
// fact<prime,cnt>
long long sod(long long n){
    long long SOD = 1LL;
    for (std::map<long long,int>::iterator it=fact.begin(); it!=fact.end(); ++it){
        if(it->first==1) SOD*=it->first+1; // to avoid overflow
        else SOD*=((Pow(it->first,it->second+1)-1)/(it->first-1));
    }
    return SOD; // proper sod = sod-n
}


// nloglogn
// a[i]=k means the sum of divisor of i is k
int a[1000099];
void sod(){
    int i,j;
    for(i=2;i<=1000000;i++){
        for(j=i;j<=1000000;j=j+i){
            a[j]=a[j]+i;
        }
    }
}


// for single number
int sod(int n) {
	int res = 1, i = 2;
	for (; i*i < n; ++i)
		if (n % i == 0)
			res += i + n/i;
	if (i*i == n)
		res += i;
	return res;
}