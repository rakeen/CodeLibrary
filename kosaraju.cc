/*
Kosaraju's 2pass algo for SCC
O(V+E)

ref:
https://github.com/PetarV-/Algorithms/blob/master/Graph%20Algorithms/Kosaraju's%20Algorithm.cpp
https://github.com/calmhandtitan/algorepo/blob/master/graph/kosaraju_SCC.cpp
*/


bool vis[MAXN];
stack<int> s;
void dfs1(int u){
    vis[u]=1;
    for(auto i:G[u]){
        int v=G[u][i];
        if(!vis[v]) dfs1(v);
    }
    s.push(u);  // store the nodes according to their finishing time
}

void dfs2(int u){
    vis[u]=1;
    cout<<u<<endl; // print the nodes in an SCC
    for(auto i:G[u]){
        int v=G[u][i];
        if(!vis[v]) dfs2(v);
    }
}

void kosaraju(){
    memset(vis,0,sizeof vis);
    for(int i=0;i<V;i++) // V is the total number of nodes
        if(!vis[i]) dfs1(i);
        
    memset(vis,0,sizeof vis);
    while(!s.empty()){
        int v=s.top();
        s.pop();
        if(!vis[v]){
            dfs2(v); // one component
        }
    }
}