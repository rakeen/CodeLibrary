/*
detecting bridge in undirected graph.

if we have a vertex v whose parent in the DFS tree is u, and no ancestor of v has a back edge pointing to it, then (u, v) is a bridge.

returns the smallest DFS number that has a back edge pointing to it
in the DFS subtree rooted at u

ref: http://www.cs.cornell.edu/~wdtseng/icpc/notes/graph_part1.pdf
*/
int dfs(int u,int p){
    color[u]=1;
    dfsNum[u]=num++;
    int leastAncestor=num;
    for(auto i:G[u]){
        int v=G[u][i];
        if(v==p) continue;
        if(color[v]==0){
            int rec = dfs(v,u);
            if(rec>dfsNum[u])
                cout<<"Bridge: "<<u<<" "<<v<<endl;
            leastAncestor = min(leastAncestor,rec);
        }
        else{
            leastAncestor = min(leastAncestor,dfsNum[v]);
        }
    }
    color[u]=2;
    return leastAncestor;
}




/*
more practical approach of printing bridges
ref: https://github.com/calmhandtitan/algorepo/blob/master/graph/bridges.cpp
*/
int low[MAXN],arrivalTime[MAXN]; // reset to 0
bool vis[MAXN]; //reset to 0
int parent[MAXN]; /// reset to -1

void bridge(int u){
    static int time=0; // doesn't change when recursed?
    vis[u]=1;
    arrivalTime[u]=low[u]=time++;
    for(auto i:G[u]){
        int v=G[u][i];
        if(!vis[v]){
            parent[v]=u;
            bridge(u);
            low[u]=min(low[u],low[v]);
            if(low[v]>arrivalTime[u]) cout<<"Bridge found: "<<u<<" "<<v<<endl;
            else if(v!=parent[u]) low[u]=min(low[u],arrivalTime[v]);
        }
    }
}



/*
detecting bridge in undirected graph
ref: https://github.com/calmhandtitan/algorepo/blob/master/graph/checkBridge.cpp
*/
void dfs(int u){
    vis[u]=1;
    for(auto i:G[u]) if(!vis[G[u][i]]) dfs(G[u][i]);
}

// returns the total number of node that is reachable from node x
int totNode(int x){
    int cnt=0;
    dfs(x);
    l(i,n)      // total number of nodes
        if(vis[i]) cnt++;
    return cnt;
}
// Edge a-b
if(totNode(a)!=totNode(b)) cout<<"Bridge found: "<<a<<"-"<<b<<endl;