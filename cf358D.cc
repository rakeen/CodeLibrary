/**
CF 682 D
CF round#358-D

Given two strings of length n and m find the maximum length of the sequence p1p2...pk
such that a1p1a2p2... akpkak + 1 and b1p1b2p2... bkpkbk + 1
where ai is a strign and may be empty.
e.g.
|bba|aa|b|a|bb
ab|bba|bb|aa|a|b|a

9 12 4
bbaaababb
abbbabbaaaba
*/

#include <bits/stdc++.h>

using namespace std;

string a,b;
int mem[1002][1002][11][2];

int dp(int i,int j,int k,int match){
	if(i>=a.size()) return 0;
	if(j>=b.size()) return 0;
	if(mem[i][j][k][match]!=-1) return mem[i][j][k][match];

	int ret=0;
	if(match){
		if(a[i]==b[j]){
			ret=max(ret,dp(i+1,j+1,k,match)+1);
		}
		ret=max(ret,dp(i+1,j,k,0));
		ret=max(ret,dp(i,j+1,k,0));
	}
	else{
		if(a[i]==b[j] && k>0){
			ret=max(ret,dp(i+1,j+1,k-1,1)+1);
		}
		ret=max(ret,dp(i+1,j,k,0));
		ret=max(ret,dp(i,j+1,k,0));
	}
	return mem[i][j][k][match]=ret;
}
int main(){
	int n,m,k;
	cin>>n>>m>>k;
	cin>>a>>b;
	memset(mem,-1,sizeof(mem));
	int ans = dp(0,0,k,0);
	cout<<ans<<endl;
	return 0;
}