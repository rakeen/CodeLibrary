/*
LOJ 1060 nth-permutation

find the n-th permutation   n=1e9
*/
#include <bits/stdc++.h>

using namespace std;

int main(){
	int t;
	cin>>t;
	for(int T=1;T<=t;T++){
		cout<<"Case "<<T<<": ";

		string s;
		int n;
		cin>>s>>n;

		long long fact[30];
		fact[0]=1;
		for(int i=1;i<30;i++) fact[i]=fact[i-1]*i;


		int freq[30];
		memset(freq,0,sizeof freq);
		for(int i=0;i<s.size();i++) freq[s[i]-'a']++;

		long long valid = fact[s.size()];
		for(int i=0;i<26;i++) valid/=fact[freq[i]];

		if(valid<n){
			cout<<"Impossible"<<endl;
		}
		else{
			int sz = s.size();

			while(sz){
				valid=fact[sz-1];

				for(int i=0;i<26;i++){ // for all the char in the string
					if(freq[i]){
						for(int j=0;j<26;j++){ //find the magic num
							if(i==j) valid/=fact[freq[i]-1];
							else valid/=fact[freq[j]];
						}

						if(valid>=n){
							// found the char! go to next pos
							cout<<char(i+'a');
							sz--;
							freq[i]--;
							break;
						}
						else{
							// this is not the char
							// subtract n-valid to go to the next smallest perm
							n-=valid;
							valid=fact[sz-1]; // set the new fact
						}
					}
				}
			}
			cout<<endl;
		}
	}

	return 0;
}