// ref: http://zobayer.blogspot.com/2009/08/calculate-ncr-using-dp.html

long long memo[MAX][MAX];

long long nCr(long long n, long long  r){
    if(n==r) return memo[n][r] = 1;
    if(r==0) return memo[n][r] = 1;
    if(r==1) return memo[n][r] = (long long)n;

    if(memo[n][r]) return memo[n][r];

    return memo[n][r] = nCr(n-1,r) + nCr(n-1,r-1);
}


/*
iterative
*/
l(i,1010) ncr[i][i]=1,ncr[i][0]=1;
for(int i=2;i<1010;i++){
	for(int j=1;j<i;j++){
		ncr[i][j]=(ncr[i-1][j]+ncr[i-1][j-1])%MOD;
	}
}




// when M is a prime and M>n,r
// M must be bigger. otherwise when you calc fact[i] it'll become 0 at one point.
// ref: https://comeoncodeon.wordpress.com/tag/lucas/

/// Fermat's Little Theorem
/// only when m is a prime
/// more info: http://is.gd/ZPL5aJ
// a^b % m
LL Pow(LL a,LL b,LL m){

    if(b==0) return 1;
    else if(b%2==1) return (a*Pow(a,b-1,m)%m )%m;
    else{
        LL tt = Pow(a,b/2,m) % m;
        return ((tt%m)*(tt%m))%m; // working in int until you're typecasting
    }
}
// (1/a)%m
LL modInv(LL a,LL m){
    return Pow(a,m-2,m); // Fermat
}// log(m)

#define LIM 1000000
#define MOD 1000000007
LL fact[LIM];
void fact_ini(){
    fact[0]=1;
    fact[1]=1;
    for(LL i=2;i<=LIM;i++){
        fact[i]=(fact[i-1]*i)%MOD;
    }
}

// https://www.quora.com/How-do-I-find-the-value-of-nCr-1000000007-for-the-large-number-n-n-10-6-in-C
LL nCr(LL n,LL r,LL mod){
    return (fact[n]*( (modInv(fact[r], mod) * modInv(fact[n-r], mod)) % mod)) % mod;
}





// Lucas theorem when MOD is a small prime
// ref: https://comeoncodeon.wordpress.com/2011/07/31/combination/
LL SmallC(int n, int r, int MOD){
    // memory efficient | uses only two row of the pascal triangle!
    vector< vector<long long> > C(2,vector<long long> (r+1,0));

    for (int i=0; i<=n; i++){
        for (int k=0; k<=r && k<=i; k++)
            if (k==0 || k==i)
                C[i&1][k] = 1;
            else
                C[i&1][k] = (C[(i-1)&1][k-1] + C[(i-1)&1][k])%MOD;
    }
    return C[n&1][r];
}

LL Lucas(int n, int m, int p){
    if (n==0 && m==0) return 1;
    int ni = n % p;
    int mi = m % p;
    if (mi>ni) return 0;
    return Lucas(n/p, m/p, p) * SmallC(ni, mi, p);
}







// nCr % M  where, M = p^a , p is a prime
// Lucas with "strange factorial"


// find n!%MOD
long long factMOD(int n, int MOD){
    long long res = 1;
    while (n > 0){
        for (int i=2, m=n%MOD; i<=m; i++)
            res = (res * i) % MOD;
        if ((n/=MOD)%2 > 0)
            res = MOD - res;
    }
    return res;
}

// returns the power of p in n!
int countFact(int n, int p){
    int k=0;
    while (n>=p){
        k+=n/p;
        n/=p;
    }
    return k;
}

long long C(int n, int r, int MOD){
    if (countFact(n, MOD) > countFact(r, MOD) + countFact(n-r, MOD))
        return 0;

    return (factMOD(n, MOD) *
            ((InverseEuler(factMOD(r, MOD), MOD) *
            InverseEuler(factMOD(n-r, MOD), MOD)) % MOD)) % MOD;
}





