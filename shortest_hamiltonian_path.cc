// UvA 11813 	| Shopping 	| shortest hamiltonian path
/**
given a graph and a certain checkpoints(10) find out the shortest route that starts and ends with the same node
lim: 1e5
*/

#include <bits/stdc++.h>
using namespace std;
#define l(i,n) for(int i=0;i<n;i++)

typedef long long LL;
#define pii pair<LL,LL>
#define MAX 100100

vector<pii> E[MAX+1];
vector<int> checkPoint;

int S; // num of checkPoint

LL cost[12][12];

void dijkstra(int idx){
    int src=checkPoint[idx];

    priority_queue<pii> q;
    q.push({0,src});

    bool vis[MAX+1];
    LL d[MAX+1];

    memset(vis,0,sizeof(vis));
    l(i,MAX+1) d[i]=LLONG_MAX;

    d[src]=0;
    vis[src]=1;

    while(!q.empty()){
        pii u = q.top();
        q.pop();

        for(int i=0;i<E[u.second].size();i++){
            pii x = E[u.second][i];
            if(d[x.first]>d[u.second]+x.second){
                d[x.first]=d[u.second]+x.second;
                if(!vis[x.first]){
                    q.push({-d[x.first],x.first}); // reverse sort!
                    vis[x.first]=1;
                }


            }
        }
    }
    
    l(i,S) cost[idx][i]=d[checkPoint[i]];
}


LL mem[20][(1<<12)];
LL dp(int pos,int mask){
    if(mask==((1<<S)-1)) return cost[pos][0];
    if(mem[pos][mask]!=-1) return mem[pos][mask];

    LL ret=LLONG_MAX;
    l(i,S){
        if(!(mask&(1<<i)))  ret = min(ret,dp(i,mask|(1<<i))+cost[pos][i]);
    }
    return mem[pos][mask]=ret;
}

int main(){
    int t;
    cin>>t;
    while(t--){

        memset(mem,-1,sizeof(mem));
        l(i,MAX+1) E[i].clear();
        checkPoint.clear();
        memset(cost,0,sizeof(cost));

        int n,m;
        cin>>n>>m;
        l(i,m){
            int u,v,w;
            cin>>u>>v>>w;
            E[u].push_back({v,w});
            E[v].push_back({u,w});
        }

        cin>>S;
        
        S++; /// <== important

        checkPoint.resize(S);
        for(int i=1;i<S;i++) cin>>checkPoint[i];
        checkPoint[0]=0;

        l(i,S) dijkstra(i);
        LL ans = dp(0,0);
        cout<<ans<<endl;
    }
    return 0;
}