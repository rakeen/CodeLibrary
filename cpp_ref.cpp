cout<<setprecission(9)<<fixed<<var<<endl;
cout<<setw(3)<<"abc"<<endl; // width of the padding. default justification is left. if num if bigger it doesn't cut off.
cout<<setfill('*')<<endl;   // set the fill char for padding
cout<<hex<<showbase<<left<<42<<endl;  // right padding fill. output: 0x2a********

cout<<bitset<8>(7)<<endl;   // prints bin of 7

bitset<4> bts;
bts.set(); // 1111
bts.set(2,0); // 1011   pos 2 with 0
bts.set(2);  // 1111 pos 2 with 1
bts.flip(); // 0000

bitset<4> b (string("0001"));
b.flip(2); // 0101
b.flip() ; // 1010

isalpha(char)    isdigit(char)   isalnum(char)   atoi()  toupper(char)

int num[]={10,20,30};
accumulate(num,num+3,0); // init with 0
accumulate(num,num+3,minus<int>()): // minus func

// size_t is any unsigned type 
DO NOT USE size_t  
it takes more time.
for proof see PKU/POJ 1811


// string to number and sscanf shit!
sscanf(&n[strlen(n)-x],"%lld",&xx);
string y=n.substr(n.size()-x);
LL xx =atoll(y.c_str());


// stringstream
string c,x;
getline(cin,x);
stringstream ss(x);
ss>>c;



__builtin_ctz()   traiing zero
__builtin_clz()   leading zero


DBL_MAX,LLONG_MAX
cin.ignore()     for getline() after taking int cin
typeid(var_name/expression).name()



const double TIME_LIMIT = 0.95;
clock_t time_start;
void time_cut_start(){time_start = clock();}
bool check_time_cut(){return 1. * (clock()-time_start) / CLOCKS_PER_SEC < TIME_LIMIT;}

time_cut_start();
srand(time(NULL));
while(check_time_cut()){
    random_shuffle(d,d+SIZE); // d is an array
}



vector.resize()
v.pop_back()
v.back()    // returns the last element
vii.emplace_back(f, s); // push pair into vector of pair
sort(all(v),greater<int>())
rotate(myvector.begin(),myvector.begin()+3,myvector.end());   makes the middle param the first one and does a circular shift

// don't use memset on non (plain old data)POD types. http://stackoverflow.com/q/1975916
fill(v.begin(), v.end(), 0); 
fill_n(v.begin(),v.size(),0);


set:
====
remember you can't dynamically access the elements of set. you need to iterate and that's not O(1)

for (std::set<int>::iterator it=myset.begin(); it!=myset.end(); ++it)
    std::cout << ' ' << *it;


set.find(x) // returns a pointer, if not found returns set.end()

map iterator:
=============
for (std::map<char,int>::iterator it=mymap.begin(); it!=mymap.end(); ++it)
for(__typeof(a.begin()) i=a.begin();i!=a.end();i++)

tuple<int,int,int,int> tp;

hypot(x,y)       returns the hypoteneus

do{
    std::cout << myints[0] << ' ' << myints[1] << ' ' << myints[2] << '\n';
}while ( std::next_permutation(myints,myints+3) );

mp.count(x)     // returns true if x is in mp otherwise positive number


struct:
======

struct node{
    int x,y,w;
    friend bool operator <(node a,node b){
        return a.w>b.w;
    }
};
struct Point{
	double x,y;
	point(){}
	bool operator <(const Point &p) const {
		return x < p.x || (x == p.x && y < p.y);
	}
}

