// ref: http://qr.ae/RU9J9o
// log(N)

unsigned long long russianPeasant(unsigned long long a, unsigned long long b, unsigned long long c) {
    unsigned long long ret=0;
    while(b) {
        if(b&1) {
            ret += a;
            ret %= c;
        }
        a *= 2;
        a %= c;
        b /= 2;
    }
    return ret;
}