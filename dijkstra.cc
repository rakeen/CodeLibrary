/*
single source shortest path SSSP
in a directed graph if there's no cycle it becomes a tree
for longest path find shortest path of -G
*/

// aka spfa | nlog(n)
LL d[MAX+1],parent[MAX+1];

void dijkstra(int src){
    l(i,MAX+1) d[i]=LLONG_MAX;
    
    priority_queue<pii> q;
    // priority_queue<pii,vector<pii>,greater<pii> > q; // and forget abt the reverse srt trick
    
    q.push({0,src});
    d[src]=0;
    parent[src]=-1;
    
    while(!q.empty()){
        pii u = q.top();
        q.pop();
        for(int i=0;i<E[u.second].size();i++){
            pii v = E[u.second][i];
            if(d[v.second]>d[u.second]+v.first){
                d[v.second]=d[u.second]+v.first;
                parent[v.second]=u.second; // u is the parent of v
                q.push({-d[v.second],v.second}); // reverse sort!
            }
        }
    }
}

void printPath(int idx){
	if(parent[idx]==-1) return;

	printPath(parent[idx]);
	cout<<idx<<" "; // printing
}