//ref: http://qr.ae/RU9Jgx
// log(N)

long long mulmod(long long a,long long  b,long long c) {
    if (a == 0 || b == 0) return 0;
    if (a == 1) return b%c;
    if (b == 1) return a%c;
    
    long long a2 = mulmod(a, b / 2, c);
    if ((b & 1) == 0)    return (a2 + a2) % c;
    else    return ((a % c) + (a2 + a2)) % c;
}

// iterative approach is faster
long long mulmod(long long a, long long b, long long c)  {
    long long exp = a%c;
    long long res=0;
    while(b){
        if(b&1LL){
            res+=exp;
            if(res>c) res-=c;
        }
        exp<<=1LL;
        if(exp>c) exp-=c;
        b>>=1LL;
    }
    return res;
}