/**
DSU/Union-Find
O(logn) (with union by rank and path compression)
O(n)	(without rank and path compression)

can detect connected components,cycles in graph
*/


int find(int a){
	if(a==parent[a]) return a;
	return parent[a]=find(parent[a]); // path compression
}

int union(int a,int b){
	int x = parent[a];
	int y = parent[b];
	
	// union by rank
	if(rank[x]<rank[y]) parent[x]=y;
	else if(rank[y]<rank[x]) parent[y]=x;
	else{
		parent[x]=y;
		rank[y]++;
	}
}