Warnings:
=========

- If your type if *signed LL* and you try to fit in **(1<<64)**, it'll **overflow**. So declare your variable unsigned when necessary. **Ref Problem: UvA 10446**
- Always declare big array **globally**. Judging System might cause RTE(i.e spoj)  
- becareful about using double. even 299.95*100 gives 29994  ref: uva147  
- check if you've **defined** something but didn't assign any value to it!  
- check if you've any local variable same as global variable  
- use minus(-) to sort the array in descending order! i.e queue<int> p.push(-value)