vector<int> g[1010];

bool vis[1010];
bool in_stack[1010];
bool loop=0;

void dfs(int src){
	vis[src]=1;
	in_stack[src]=1;

	l(i,g[src].size()){
		if(!vis[g[src][i]]) dfs(g[src][i]);
		if(in_stack[g[src][i]]) loop=1;
	}
	in_stack[src]=0;
}

/**
since it is a directed graph, you can reach a node by more than 1 path. so if you just use vis[]
it won't work. eg:
1->2->3
1->3
this is a false cycle
Only vis[] will treat back edge and cross edge the same way.
--------------
ref:
http://disq.us/p/1831ygj
UvA 12804
*/





/*
Undirected graph
O(E+V)
works on self loop
*/
bool dfs(int u,int p){
    vis[u]=1;
    for(auto i:G[u]){
        int v=G[[u][i];
        if(!vis[v]) if(dfs(v,u)) return 1;
        else if(v!=p) return 1; //If an adjacent is visited and not parent of current vertex, then there is a cycle.
    }
    return 0;
}
bool cycleFind(){
    memset(vis,0,sizeof vis);
    for(int u=0;u<V;u++) // total number of Vertex
        if(!vis[u])
            if(dfs(u,-1)) return 1;
    return 0;
}


/*
Undirected Graph using DSU
doesn't work with self loop
O(ElogV)
*/
// pseudo-code
bool cycleFind(){
    for(auto i:E){ // iterate over all edges a-b
        int x = Find(i.first); // a of edge
        int y = Find(i.second; // b of edge
        if(x==y) return 1;
        Union(x,y);
    }
    return 0;
}