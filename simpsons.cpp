/**
adaptive Simpson's rule for integration
*/
double simps(double a, double b) {
  return (f(a) + 4*f((a+b)/2) + f(b))*(b-a)/6;
}
double integrate(double a, double b){
  double m = (a+b)/2;
  double l = simps(a,m), r = simps(m,b), tot=simps(a,b);
  if (fabs(l+r-tot) < eps) return tot;
  return integrate(a,m)+integrate(m,b);
}


/**
optimized
*/
inline double simpson(double fl,double fr,double fmid,double l,double r) { return (fl+fr+4.0*fmid)*(r-l)/6.0; }
double rsimpson(double slr,double fl,double fr,double fmid,double l,double r)
{
	double mid = (l+r)*0.5;
	double fml = f((l+mid)*0.5);
	double fmr = f((mid+r)*0.5);
	double slm = simpson(fl,fmid,fml,l,mid);
	double smr = simpson(fmid,fr,fmr,mid,r);
	if(fabs(slr-slm-smr) < eps) return slm+smr;
	return rsimpson(slm,fl,fmid,fml,l,mid)+rsimpson(smr,fmid,fr,fmr,mid,r);
}
double integrate(double l,double r)
{
	double mid = (l+r)*.5;
	double fl = f(l);
	double fr = f(r);
	double fmid = f(mid);
	return rsimpson(simpson(fl,fr,fmid,l,r),fl,fr,fmid,l,r);
}


/*
Related problems:
http://www.spoj.com/problems/CIRU/
http://acm.timus.ru/problem.aspx?space=1&num=1562
https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&category=572&page=show_problem&problem=4146
https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=3893
http://acm.sgu.ru/problem.php?problem=217
http://acm.zju.edu.cn/onlinejudge/showProblem.do?problemCode=2675
*/

// ref: http://codeforces.com/blog/entry/8242