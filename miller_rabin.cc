//ref: http://qr.ae/RU9Jgx
// log(N)
long long mulmod(long long a,long long  b,long long c) {
    if (a == 0 || b == 0)   return 0;
    if (a == 1) return b%c;
    if (b == 1) return a%c;

    long long a2 = mulmod(a, b / 2, c);

    if((b & 1) == 0)   return (a2 + a2) % c;
    else    return ((a % c) + (a2 + a2)) % c;
}


// a^n%m
long long Pow(long long x,long long n,long long mod){
    if(n==1LL)return x%mod;
    x%=mod;
    long long base=x;
    long long ret=1LL;
    while(n){
        if(n&1LL) ret=mulmod(ret,base,mod);
        base=mulmod(base,base,mod);
        n>>=1LL;
    }
    return ret;
}


// n−1 = 2^s * d with d odd by factoring powers of 2 from n−1
bool witness(long long n, long long s, long long d, long long a){
    long long x = Pow(a, d, n);
    long long y;
 
    while(s){
        y = mulmod(x,x,n);
        if (y == 1 && x != 1 && x != n-1)   return false;
        x = y;
        --s;
    }
    if(y != 1)  return false;
    return true;
}


// v1 accurate atleast 2^64
// ref: https://miller-rabin.appspot.com/#record_history

long long mr_bases[7]={2, 325, 9375, 28178, 450775, 9780504, 1795265022};
bool is_prime_mr(long long n){
    if (((!(n & 1)) && n != 2 ) || (n < 2) || (n % 3 == 0 && n != 3))
        return false;
    if (n <= 3)
        return true;
 
    long long d = n / 2;
    long long s = 1;
    while (!(d & 1)) {
        d /= 2;
        ++s;
    }

    // iter ref: http://www.cc.gatech.edu/grads/n/nbalani3/codes/Miller%20Rabin%20Primality%20Test.cpp 
    for(int i=0;i<7;i++){
        if(mr_bases[i]<n){
            if(!witness(n, s, d, mr_bases[i])) return false;
        }
    }
    return true;
}




// v2 deterministic upto 341,550,071,728,321
/// ref: https://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#C
/*
 * if n < 1,373,653, it is enough to test a = 2 and 3;
 * if n < 9,080,191, it is enough to test a = 31 and 73;
 * if n < 4,759,123,141, it is enough to test a = 2, 7, and 61;
 * if n < 1,122,004,669,633, it is enough to test a = 2, 13, 23, and 1662803;
 * if n < 2,152,302,898,747, it is enough to test a = 2, 3, 5, 7, and 11;
 * if n < 3,474,749,660,383, it is enough to test a = 2, 3, 5, 7, 11, and 13;
 * if n < 341,550,071,728,321, it is enough to test a = 2, 3, 5, 7, 11, 13, and 17.
 */
bool is_prime_mr(size_t n){
    if (((!(n & 1)) && n != 2 ) || (n < 2) || (n % 3 == 0 && n != 3))
        return false;
    if (n <= 3)
        return true;
 
    size_t d = n / 2;
    size_t s = 1;
    while (!(d & 1)) {
        d /= 2;
        ++s;
    }
 
    if (n < 1373653)
        return witness(n, s, d, 2) && witness(n, s, d, 3);
    if (n < 9080191)
        return witness(n, s, d, 31) && witness(n, s, d, 73);
    if (n < 4759123141)
        return witness(n, s, d, 2) && witness(n, s, d, 7) && witness(n, s, d, 61);
    if (n < 1122004669633)
        return witness(n, s, d, 2) && witness(n, s, d, 13) && witness(n, s, d, 23) && witness(n, s, d, 1662803);
    if (n < 2152302898747)
        return witness(n, s, d, 2) && witness(n, s, d, 3) && witness(n, s, d, 5) && witness(n, s, d, 7) && witness(n, s, d, 11);
    if (n < 3474749660383)
        return witness(n, s, d, 2) && witness(n, s, d, 3) && witness(n, s, d, 5) && witness(n, s, d, 7) && witness(n, s, d, 11) && witness(n, s, d, 13);
    return witness(n, s, d, 2) && witness(n, s, d, 3) && witness(n, s, d, 5) && witness(n, s, d, 7) && witness(n, s, d, 11) && witness(n, s, d, 13) && witness(n, s, d, 17);
}