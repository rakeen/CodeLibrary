//ref: https://en.wikipedia.org/wiki/Newton%27s_method

// f(x)= actual function
// fd(x)=derivative of f(x)
double newton(){
  if (f(0)==0) return 0;
  for (double x=0.5; ;){             // initial guess x = 0.5
    double x1 = x - f(x)/fd(x);      // x1 = next guess
    if (abs(x1-x) < eps) return x;  // the guess is accurate enough
    x = x1;
  }
}


// related problem:
// https://uva.onlinejudge.org/external/103/10341.pdf